$(document).ready(function() {
  $.getJSON( "data/data.json", function(data) {
    
    $(data).each(function(i) {
      if (this.veta.length>0) {$("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa")};
      if (this.veta.length>0) {$(".respondent").last().append("<div class='cedulka'>Klikněte pro celou odpověď</strong></div>")};
      if (this.veta.length==0) {$("<div class='respondent neodpovedel'></div>").appendTo("#anketa")};
      $(".respondent").last().append("<img class='portret' src='" + this.foto + "'>");
      $(".respondent").last().append("<p class='jmeno'><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.strana + "</p>");
      if (this.veta.length>0) {$(".respondent").last().append("<p class='veta'>&bdquo;" + this.veta + "&ldquo;</p>")};
      if (this.veta.length==0) {$(".respondent").last().append("<p class='veta' style='color:red;'>Bez odpovědi</p>")};
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><p><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.strana + "</p><p><em>1. Registr smluv byl prezentován jako jeden ze zásadních protikorupčních nástrojů. Podle řady odborníků ale registr nyní, po schválení výjimky pro státní podniky, postrádá význam a otevírá možnost pro netransparentní nakládání s veřejnými prostředky. Souhlasíte s tím?</em></p><p>" + this.odpoved1 + "</p><p><em>2. Z jakého důvodu jste pro novelu hlasoval/a?</em></p><p>" + this.odpoved2 + "</p></div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        $(this).find(".cedulka").hide();
      }
    );
    
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

  });
});