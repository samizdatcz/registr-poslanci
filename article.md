---
title: "ANKETA: Proč 80 poslanců &bdquo;okleštilo&ldquo; registr smluv? Je to složité, obhajují novelu zákonodárci"
perex: "Nejprve stačil jeden hlas, aby neprošla. Poté zase o jeden hlas prošla. Málokterá novela zákona měla tak pozoruhodný vývoj jako ta k registru smluv. Zpravodajský web Českého rozhlasu proto oslovil všech osmdesát poslanců, kteří schválili výjimku ze zveřejňování smluv pro národní pivovar Budvar a státní firmy. Odpovědělo 23 z nich."
description: "Zpravodajský web Českého rozhlasu oslovil všech osmdesát poslanců, kteří schválili výjimku ze zveřejňování smluv pro národní pivovar Budvar a státní firmy. Odpovědělo 23 z nich."
authors: ["Hana Mazancová"]
published: "27. února 2017"
socialimg: http://media.rozhlas.cz/_obrazek/3782564--registr-smluv--1-950x0p0.jpeg
url: "registr-poslanci"
libraries: [jquery]
styles: ["styl/anketa.css", "styl/magnific-popup.css"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1701187
    title: Budvar dostal výjimku. Firmy vlastněné státem, kraji nebo obcemi nemusí zveřejňovat smlouvy
    perex: Výjimku ze zveřejňování smluv by měly vedle Budějovického Budvaru dostat i všechny státní podniky a společnosti s většinovou účastí státu, krajů nebo obcí, které mají obchodní či průmyslovou povahu.
    image: http://media.rozhlas.cz/_obrazek/3783421--poslanecka-snemovna--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1699336
    title: Budvar se odmítá v registru smluv &bdquo;svlékat před konkurencí&ldquo;. Většina firem s tím nemá problém
    perex: Státní pivovar podnikne &bdquo;potřebné kroky&ldquo;, aby se na něj vztahovala výjimka pro zveřejňování smluv, kvůli kterému má prý řadu problémů.
    image: http://media.rozhlas.cz/_obrazek/2704763--nove-sklenice-budejovickeho-budvaru--1-1239x929p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1701687
    title: Jak prošlo osekání registru smluv? Pomohla i klička s &bdquo;vypnutými&ldquo; poslanci z ČSSD
    perex: K osekání klíčové protikorupční iniciativy přispěla i trojice poslanců z ČSSD, která před opakovaným závěrečným hlasováním odešla ze sálu.
    image: http://media.rozhlas.cz/_obrazek/3634971--poslancka-snemovna-2452016-jan-hamacek-a-vojtech-filip--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1701341
    title: Přehledně: kastrace, facka lidem i důvody pro nehlasování. Co řekli politici o výjimce v registru?
    perex: Vykastrování zákona, pokračování plýtvání veřejnými prostředky, potřeba odborného dohledu v dozorčích radách. To jsou některé reakce na odhlasování výjimky ze zákona o registru smluv.
    image: http://media.rozhlas.cz/_obrazek/3782564--registr-smluv--1-950x0p0.jpeg
---

Výjimku ze zveřejňování smluv v online registru by měly spolu s národním podnikem Budějovický Budvar získat i všechny státní firmy a společnosti, kde má většinovou účast stát, kraje nebo obce. Minulý týden o tom [rozhodla Poslanecká sněmovna](http://www.rozhlas.cz/zpravy/domaci/_zprava/1701187).

Novelu zákona schválili poslanci o jeden hlas, a to až napodruhé. Pro připomenutí: při prvním hlasování novela právě o jeden hlas neprošla. Z tehdy přítomných 161 zákonodárců pro mírnější podobu zvedlo ruku jen 80. V tu chvíli se ale proti výsledku ohradil předseda poslaneckého klubu ČSSD Roman Sklenák.
 
"Hlasoval jsem v tomto posledním hlasování ‚Ano‘ a na záznamu mám ‚Zdržel se‘. Zpochybňuji hlasování," oznámil Sklenák kolegům, kteří jeho námitku uznali. Jenže pro druhé hlasovací kolo, které se odehrálo hned vzápětí, se nepřihlásilo celkem pět poslanců sociální demokracie, ač předtím to byli pouze dva. V tu chvíli tak stačilo, aby v novém hlasování pro novelu zvedlo ruku 80 poslanců. A přesně to se také stalo. Novela tak byla přijata nejnižším možným počtem hlasů.
 
Proč se tři poslanci sociální demokracie - Antonín Seďa, Pavlína Nytrová a ministr pro legislativu a lidská práva Jan Chvojka - v závěrečném hlasování zdrželi, [si můžete přečíst zde](http://www.rozhlas.cz/zpravy/domaci/_zprava/1701687).

## Výjimka od Jurečky

Původní návrh novely k registru smluv, kterou předložil ministr zemědělství Marian Jurečka (KDU-ČSL), počítal s výjimkou pouze pro národní pivovar Budvar. Poslanci k němu ale nakonec připojili desítky dalších pozměňovacích návrhů.
 
A právě to kritizovala iniciativa Rekonstrukce státu. "Výjimka pro národní podnik Budvar (...) se však brzy stala nosičem pro desítky dalších pozměňovacích návrhů. Na konci všeho je výrazně okleštěný zákon," napsala iniciativa v tiskové zprávě. Podle jejích zástupců nyní z registru zmizí kontrakty v hodnotě 148 miliard ročně.
 
Zvrátit novelu se v Senátu pokusí KDU-ČSL. Předseda strany Pavel Bělobrádek, jehož pět poslanců pomohlo návrh prosadit, v pátek [na Twitteru uvedl](https://twitter.com/PavelBelobradek/status/835107196035792896), že strana bude chtít do registru vrátit povinnost zveřejňování kontraktů u státních firem.
 
Zpravodajský web Českého rozhlasu oslovil všech 80 poslanců, kteří pro novelu zákona o online registru hlasovali. Na dvě krátké anketní otázky odpovědělo 23 z nich.
 
„Budvar a nejen on byl ohrožen,“ vysvětluje své hlasování poslanec Jiří Běhounek z ČSSD. Jeho kolega z KSČM Miroslav Opálka se podle svých slov naopak snažil napravit „nekvalitní zákon“. „Snaha Rekonstrukce státu protlačit za každou cenu nevyvážený návrh ohrozila rovné podmínky pro podnikání podniků v různém vlastnictví,“ uvedl v anketě.
 
Jan Zahradník, který z hlasujících poslanců ODS odpověděl jako jediný, zase uvedl, že svým hlasem pro novelu zákona o registru smluv volil prý „systematický přístup“.

<aside class="big">
  <div id="anketa">
   <h2 data-bso=1 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">1. Registr smluv byl prezentován jako jeden ze zásadních protikorupčních nástrojů. Podle řady odborníků ale registr nyní, po schválení výjimky pro státní podniky, postrádá význam a otevírá možnost pro netransparentní nakládání s veřejnými prostředky. Souhlasíte s tím?</h2>
   <h2 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">2. Z jakého důvodu jste pro novelu hlasoval/a?</h2>
   <h3 style="margin-left: 25px; margin-right: 25px; margin-bottom: 20px; margin-top: 30px; color: #606060">Kliknutím na respondenta zobrazíte jeho celé odpovědi</h3>
  </div>
</aside>
